﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApp1
{
    public class ReversalSort
    {
        public static List<int> GetReversalsToSort(int[] a)
        {
            List<int> r = new List<int>();

            int max = a.Max();

            //lenght to reverse
            int len = a.Length;


            while (len > 1) 
            {
                max = a.Take(len).Max();
                while ((a[len - 1] == max) && (len > 1)) 
                {
                    len--;
                    max = a.Take(len).Max();
                }

                //position of current max
                int p = 1 + Array.IndexOf(a, max);

                if (p != 1)
                {
                    p = 1 + Array.LastIndexOf(a.Take(len).ToArray(), max);                        
                    r.Add(p);
                    a = Reverse(a, p);
                }
                if (len > 1)
                {
                    r.Add(len);
                    a = Reverse(a, len);
                }
                len--;
            }

            return r;
        }

        private static int[] Reverse(int[] array, int len)
        {
            /*
            int[] sub1 = new int[len];
            sub1 = array.Take(len).ToArray();
            //int[] sub2 = new int[array.Length - len];
            sub2 = 
                sub1.Reverse().ToArray();
            int[] all = new int[array.Length];
            all = sub1.Concat(sub2).ToArray();
            */
            int[] sub1 = array.Take(len).Reverse().ToArray();
            int[] sub2 = array.Skip(len).ToArray();
            int[] all = sub1.Concat(sub2).ToArray();
            return all;
        }
    }
}
